## Whisper_gui

Une interface graphique pour utiliser Whisper et retranscrire des entretiens.

```
virtualenv .
source bin/activate
pip install -r requirements.txt
# utilisation : bin/python3 ./transcribe.py -f FILE -m MODELE
```

Arguments :

- -f ou --file : le fichier à convertir : le format du fichier doit être obligatoirement en .wav
- -m ou --modele : le modèle Whisper
- -F ou --format : Votre format de sortie (uniquement sans le mode diarisation) : plain (Texte brut, par défaut), raw (sans timecode), webvtt (WebVTT), srt (SRT)
- -d ou --diarization : Activer la diarization, c’est-à-dire la reconnaissance d’interlocuteurs. En entrée, le token pour huggingface. Nécessite d’accepter les clauses de [pyannote/speaker-diarization](https://huggingface.co/pyannote/speaker-diarization-3.0)
- -s ou -speakers : le nombre d’interlocuteurs pour la diarization. Par défaut, 2.
