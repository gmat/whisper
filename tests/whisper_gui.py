#!/usr/bin/python3
# coding: utf-8
# Tout droits réservés Justin Poncet 2023
# D'après les scripts de :  https://www.css.cnrs.fr/whisper-pour-retranscrire-des-entretiens/ https://gist.github.com/ThioJoe/e84c4e649857bf0a290b49a2caa670f7
#
# Guide d'installation de Whisper : https://github.com/openai/whisper#setup
# pip install openai-whisper
# pip install setuptools-rust
# utilisation : python3 whisper.py 

import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import whisper
import time
import json

# Modele = "base" # Taille du modèle de transcription : base, tiny, large-v2
# Language = "french" # french, english, spanish


def convertir(seconds):
    h = int(seconds // 3600)
    m = int((seconds % 3600) // 60)
    s = int(seconds % 60)
    return f"{h:02d}:{m:02d}:{s:02d}"

def export_transcript(input_file, output_file, Language, Modele):
    model = whisper.load_model(Modele)
    transcription = model.transcribe(audio=input_file, language=Language, word_timestamps=True, verbose=False)
    transcription_brut = open(output_file+"_brut.txt",'w',encoding='utf-8')
    transcription_brut.write(transcription["text"])
    transcription_brut.close()
    # Enregistrement de la transcription
    with open(output_file+"_segments.txt", 'w', encoding='utf-8') as f:
        for segment in transcription["segments"]:
            start_time = convertir(segment['start'])
            end_time = convertir(segment['end'])
            f.write(f"{start_time} - {end_time}: {segment['text']}\n")
    f.close()

def gui_file_changed(button_file):
    print("File selected: %s" % button_file.get_filename())

def gui_export_file_transcript(button):
    Language = languages_combo.get_active_text()
    Modele = models_combo.get_active_text()
    input_audio = button_file.get_filename()
    output_txt = button_file.get_filename()+"_"+Modele+"_"+Language
    start = time.time()
    export_transcript(input_audio, output_txt, Language, Modele)
    end = time.time()
    elapsed = str(round(float(end - start)/60, 2))
    button_transcript.set_label(f"Fichier transcript en {elapsed} minute(s)")


window = Gtk.Window()
window.set_title("Whisper Transcript")
window.set_border_width(20)
window.connect('delete-event', Gtk.main_quit)


box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

button_file = Gtk.FileChooserButton(title="FileChooserButton")
filename = button_file.get_filename()

button_transcript = Gtk.Button(label='Transcrire')
button_quit = Gtk.Button(label='Quitter')


languages_combo = Gtk.ComboBoxText()
languages_combo.set_entry_text_column(0)
languages_combo.append_text("french")
languages_combo.append_text("english")
languages_combo.append_text("spanish")
languages_combo.set_active(0)

models_combo = Gtk.ComboBoxText()
models_combo.set_entry_text_column(0)
models_combo.append_text("base")
models_combo.append_text("medium")
models_combo.append_text("large-v2")
models_combo.set_active(0)

box.pack_start(button_file, True, True, 0)
box.pack_start(languages_combo, False, False, 0)
box.pack_start(models_combo, False, False, 0)
box.pack_start(button_transcript, True, True, 0)
box.pack_start(button_quit, True, True, 0)
window.add(box)

button_file.connect("file-set", gui_file_changed)
button_transcript.connect('clicked', gui_export_file_transcript)
#languages_combo.connect("changed", gui_export_file_transcript)
button_quit.connect('clicked', Gtk.main_quit)


window.show_all()
Gtk.main()
