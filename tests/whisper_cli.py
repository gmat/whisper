#!/usr/bin/python3
# coding: utf-8
# Tout droits réservés Justin Poncet 2023
# D'après les scripts de :  https://www.css.cnrs.fr/whisper-pour-retranscrire-des-entretiens/ https://gist.github.com/ThioJoe/e84c4e649857bf0a290b49a2caa670f7
#
# Guide d'installation de Whisper : https://github.com/openai/whisper#setup
# pip install openai-whisper setuptools-rust
# utilisation : python3 whisper.py -f FILE -m MODELE -l LANGUAGE

import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")
import sys
import whisper
import time
import json
import argparse

parser = argparse.ArgumentParser(description="Transcribe audio files with Whisper")
parser.add_argument("-f", "--file", dest="FILE", required=True, help="Audio file name")
parser.add_argument("-m", "--modele", dest="MODELE", default="tiny", help="Modele name: tiny, tiny.en, tiny,base.en, base, small.en, small, medium.en, medium, large-v1, large-v2, large")
parser.add_argument("-l", "--language", dest="LANGUAGE", default="french", help="Language name: afrikaans, albanian, amharic, arabic, armenian, assamese, azerbaijani, bashkir, basque, belarusian, bengali, bosnian, breton, bulgarian, catalan, chinese, croatian, czech, danish, dutch, english, estonian, faroese, finnish, french, galician, georgian, german, greek, gujarati, haitian creole, hausa, hawaiian, hebrew, hindi, hungarian, icelandic, indonesian, italian, japanese, javanese, kannada, kazakh, khmer, korean, lao, latin, latvian, lingala, lithuanian, luxembourgish, macedonian, malagasy, malay, malayalam, maltese, maori, marathi, mongolian, myanmar, nepali, norwegian, nynorsk, occitan, pashto, persian, polish, portuguese, punjabi, romanian, russian, sanskrit, serbian, shona, sindhi, sinhala, slovak, slovenian, somali, spanish, sundanese, swahili, swedish, tagalog, tajik, tamil, tatar, telugu, thai, tibetan, turkish, turkmen, ukrainian, urdu, uzbek, vietnamese, welsh, yiddish, yoruba")
args = parser.parse_args()

def convertir(seconds):
    h = int(seconds // 3600)
    m = int((seconds % 3600) // 60)
    s = int(seconds % 60)
    return f"{h:02d}:{m:02d}:{s:02d}"

def export_transcript(input_file, output_file, Language, Modele):
    model = whisper.load_model(Modele)
    transcription = model.transcribe(audio=input_file, language=Language, word_timestamps=True, verbose=False)
    transcription_brut = open(output_file+"_brut.txt",'w',encoding='utf-8')
    transcription_brut.write(transcription["text"])
    transcription_brut.close()
    # Enregistrement de la transcription
    with open(output_file+"_segments.txt", 'w', encoding='utf-8') as f:
        for segment in transcription["segments"]:
            start_time = convertir(segment['start'])
            end_time = convertir(segment['end'])
            f.write(f"{start_time} - {end_time}: {segment['text']}\n")
    f.close()


print("==================")
print("Audio File:", args.FILE)
print("Modele:", args.MODELE)
print("Language:", args.LANGUAGE)
print("==================")

input_audio = args.FILE
Modele = args.MODELE
Language = args.LANGUAGE
output_txt = input_audio+"_"+Modele+"_"+Language
start = time.time()
export_transcript(input_audio, output_txt, Language, Modele)
end = time.time()
elapsed = str(round(float(end - start)/60, 2))
print(f"Fichier transcript en {elapsed} minute(s)")
print("Exported files:", output_txt+"_brut.txt", output_txt+"_segments.txt")
